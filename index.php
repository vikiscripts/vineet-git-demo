<!DOCTYPE html>
<html>
	<head>
		<title>Dashboard Shift Management</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/custom.css">
	</head>
	<body>
		<div class="container">
		  	<h2>Shift Management</h2>
		  	<div class="row">
			 	<div class="col col-md-6 col-lg-6">
			 		<div class="dashboard-mostEfficientshiftHeading">
			 			<h3 class="text-center" style="background-color: #fa6801;color: #fff; padding: 10px;">Most Efficient Shift</h3>
			 			<h4 class="text-center" style="font-weight: 600;color: #3a85e1;">Shift Timing - 25th Sep 2018(08:00 AM - 06:00 PM)</h4>
			 			<h5 class="pull-right"><span class="efficientTile">20 Min</span><br><br>Avg Delay</h4>
			 			<h4 class="text-center">Total Order Placed - 45</h4>
			 			<h4>Number of Associates - <b>2000</b></h4>
			 			<h4>Number of Managers - <b>1500</b></h4>
			 		</div>
			 	</div>
			 	<div class="col col-md-6 col-lg-6">
			 		<div class="dashboard-mostInEfficientshiftHeading">
			 			<h3 class="text-center" style="background-color: #fa6801;color: #fff;padding: 10px;">Most Inefficient Shift</h3>
			 			<h4 class="text-center" style="font-weight: 600;color: #3a85e1;">Shift Timing - 25th Sep 2018(06:00 PM - 08:00 AM)</h4>
			 			<h5 class="pull-right"><span class="efficientTile">45 Min</span><br><br>Avg Delay</h4>
			 			<h4 class="text-center">Total Order Placed - 48 Min</h4>
			 			<h4>Number of Managers - <b>5</b></h4>
			 			<h4>Number of Associates - <b>18</b></h4>
			 			
			 		</div>
			 	</div>
		  </div>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>