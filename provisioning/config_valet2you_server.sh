#!/bin/bash
#--------------- Install Ansible so you execute the playbook needed.
if [ -z "$(which ansible-playbook)" ]; then
  echo "ansible is not installed. Hence Installing please wait for a minute"
  sudo apt-get update -y
  sudo apt-get install software-properties-common -y
  sudo apt-add-repository ppa:ansible/ansible -y
  sudo apt-get update -y
  sudo apt-get install ansible -y
fi
#--------------- Execute Playbook
sudo ansible-playbook playbook.yml
