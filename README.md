### Steps to configure DemoTest

1. Clone the Repository on your newly created instance, use below command
```
git clone https://vikiscripts@bitbucket.org/vikiscripts/vineet-git-demo.git
```
Supply your github username and password when prompted  

#### Output  

![image](https://user-images.githubusercontent.com/38579707/49057914-b2ab0980-f227-11e8-948e-79e86d125279.png)

2. Now cd into DemoTets/Provisioning and execute `./config_valet2you_server.sh`  
  Make sure this script is executable in not run `chmod +x config_valet2you_server.sh`  
  Refer the below output screen for actual commands
  
#### Output 
![image](https://user-images.githubusercontent.com/38579707/49058108-89d74400-f228-11e8-8fba-58d6ba65d5c9.png)

3. Done!!! Your server should be up and running... now any changes in the repo will be updated when you re-run the same script i.e. `./config_valet2you_server.sh`


